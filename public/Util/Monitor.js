import * as firebaseConfig from './FirebaseConfig.js';
import { Momentum } from '../Model/Model.js';

// Initialize Firebase
firebase.initializeApp(firebaseConfig.firebaseConfig);
export const firestore = firebase.firestore();
export const secondReference = firebase.initializeApp(firebaseConfig.firebaseConfig, "Secondary");

var Momentum_Handler = Momentum.UNDEFINED;

export function Define_Momentum(param){
  Momentum_Handler = param;
}

// Não importar no login senão fica em loop infinito
firebase.auth().onAuthStateChanged(user => {
    if (!user) {
      window.location = '../login/login.html';
    } else {
      console.log(Momentum_Handler);
      Momentum_Handler.fun();
    }
});

export function logout(){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(() => {
    return firebase.auth().signOut();
  })
}
