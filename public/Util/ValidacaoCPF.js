
// CPFStatus = {
//     NILL:{code: 0}, VALIDATING:{code:1}, VALIDATED:{code:2}, INVALIDATED:{code:3}, WRONG:{code:4}
// };

var ValidacaoCPF = {
                    delegate :
                        {   handleCPFStatus: undefined, CPFStatusCode: 0,
                            verifyCPFonBase: undefined}
                    };

function fMasc(objeto,mascara) {
    obj=objeto
    masc=mascara
    setTimeout("fMascEx()",1)
}

function fMascEx() {
    obj.value=masc(obj.value)
}

function ValidaCPF(){
	var RegraValida=document.getElementById("lcpf").value;
	var cpfValido = /^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{11}))$/;
	if (cpfValido.test(RegraValida) == true)	{
        console.log("CPF Válido pelas normas da RF");
	} else	{
        console.log("CPF Inválido pelas normas da RF");
	}

    return cpfValido.test(RegraValida) == true ;
}

function mCPF(cpf){
    cpf=cpf.replace(/\D/g,"")
    cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
    cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
    cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")

    if (cpf.length >= 14){
        if (ValidaCPF()) {
            // cpfStatus = CPFStatus.VALIDATING;
            ValidacaoCPF.delegate.CPFStatusCode = 1;
            ValidacaoCPF.delegate.handleCPFStatus();

            var cpf_as_Id = cpf.replaceAll('.', '').replaceAll('-', '');
            ValidacaoCPF.delegate.verifyCPFonBase(cpf_as_Id);
        } else {
            // cpfStatus = CPFStatus.WRONG;
            ValidacaoCPF.delegate.CPFStatusCode = 4;
            ValidacaoCPF.delegate.handleCPFStatus();
        }

    } else {
        // cpfStatus = CPFStatus.NILL;
        ValidacaoCPF.delegate.CPFStatusCode = 0;
        ValidacaoCPF.delegate.handleCPFStatus();
    }

    return cpf
}