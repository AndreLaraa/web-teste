export function UserException(message, code) {
    this.message = message;
    this.code = code
    this.name = "UserException";
}