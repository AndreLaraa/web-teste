// =============================
//          LOADER
// =============================
export function showLoader() {
    $(".loader").fadeIn("slow");
}
export function hideLoader() {
    $(".loader").fadeOut("slow");
}

// hideLoader();
