var ClientStatus = {
    IMPORTADO_PLANILHA : {name: "IMPORTADO DA PLANILHA", code: "IP"},
    RECEM_CADASTRADO : {name: "RECÉM CADASTRADO", code: "RC"},
    AUDITORIA       : {name: "EM AUDITORIA", code: "EA"},
    COBRANCA       : {name: "EM COBRANCA", code: "EC"},
    COBRANCA_RETORNO       : {name: "RETORNADO PELA COBRANÇA", code: "RPC"},
    AUDITORIA_RETORNO       : {name: "RETORNADO PELA AUDITORIA", code: "RPA"},
    COBRANCA_SUCESSO       : {name: "COBRADO COM SUCESSO", code: "CS"},
    COBRANCA_FALHA       : {name: "FALHA NA COBRANÇA", code: "CF"},
    CORRIGIDO_VENDA     : {name: "CORREÇÃO EFETUADA", code: "CE"}
};

var firebaseConfig = {
    apiKey: "AIzaSyDhYuI_h3XSVH-653MjH8BSQSWp_bqv59A",
    authDomain: "teste-web21.firebaseapp.com",
    projectId: "teste-web21",
    storageBucket: "teste-web21.appspot.com",
    messagingSenderId: "247710352014",
    appId: "1:247710352014:web:0c41ea571621f54e663fdf"
  };

// ====================================
//  Initialize Firebase and elements
// ====================================
firebase.initializeApp(firebaseConfig);
var firestore = firebase.firestore();

$("#filter_modal_content").load("../../SharedContent/Filter.html");

var filtros = [{key:"today"},
                {key:"all_sellers", sellers:undefined},
                {key:"all_status", status: undefined}
            ];

// var sellers = [];
showLoader();
handleStatusBadge(undefined);

firebase.auth().onAuthStateChanged(user => {
    if(user) {
        getSellers();
        getAllClients();
    } else {
        hideLoader();
        window.location = '../login/login.html';
    }
});

function logout(){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(() => {
    return firebase.auth().signOut();
  })
}


function getSellers(){
    sellers = [];
    $("#filtro_seller_body").show();
    firestore.collection("employees").get()
    .then(snapshot => {
        var html_allSellers = ''
        snapshot.forEach(doc => {
            var seller = doc.data();
            // sellers.push(seller);
            html_allSellers = html_allSellers + '<div class="form-check form-check-inline">';
            html_allSellers = html_allSellers + '<input class="form-check-input" type="checkbox" name="checkbox_sellers" id="checkbox_'+doc.id+'" value="'+seller.name+'">';
            html_allSellers = html_allSellers + '<label class="form-check-label" for="checkbox_'+doc.id+'">'+seller.name+'</label>';
            html_allSellers = html_allSellers + ' </div>';
        });
        var sellers = document.getElementById('pinch_seller_div');
        sellers.innerHTML = html_allSellers;
        console.log("Funcionarios carregados: "+sellers);
    })
    .catch(error =>{
        console.log("Falha ao carregar funcionarios "+error);
    });
}

function showModalToCPF(cpf) {

    $('#staticBackdrop').modal('show');
    $("#badge_base_antiga").hide();
    $("#initializeAuditing").hide();

    firestore.collection("clientes").doc(cpf).onSnapshot(function(doc) {
        var data =  doc.data();

        handleStatusBadge(data.status);

        var inputs = document.getElementsByClassName("verificado_status");
        inputs.forEach(function(input) {
            input.disabled = true;
        });
        switch (data.status) {
            case ClientStatus.IMPORTADO_PLANILHA.code:
            case ClientStatus.COBRANCA.code:
            case ClientStatus.COBRANCA_SUCESSO.code:
            case ClientStatus.COBRANCA_RETORNO.code:
            case ClientStatus.CORRIGIDO_VENDA.code:
            case ClientStatus.COBRANCA_FALHA.code:
                inputs.forEach(function(input) {
                    input.disabled = false;
                });
                break;
            default:
                // $("#initializeAuditing").show();
                break;
        }

        var formattedCPF = data.cpf;
        formattedCPF = formattedCPF.replace(/[^\d]/g, "");
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");

        var _date = new Date(data.data_nascimento_segurado.seconds * 1000);
        let options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var date = _date.toLocaleString('pt-BR', options);

        console.log(data);
        document.getElementById('lsexo').value=data.sexo;
        document.getElementById('lenderecoEstado').value=data.endereco_estado;
        document.getElementById('lbandeiraCartao').value=data.bandeira_cartao;

        $('#modal_client_name').html(data.nome);
        $("#modal_client_cpf").html(formattedCPF);
        $("span[name='key-cpf']").html(doc.id);
        $("input[name='lnome-do-cliente']").val(data.nome);
        $("input[name='lcpf']").val(formattedCPF);

        $("input[name='lnome']").val(data.nome);
        $("input[name='ltelefone']").val(data.telefone);
        $("input[name='ldatanascimento']").val(date);
        $("input[name='lendereco']").val(data.endereco_rua);
        $("input[name='lenderecoNumero']").val(data.endereco_numero);
        $("input[name='lenderecoCEP']").val(data.endereco_cep);
        $("input[name='lenderecoBairro']").val(data.endereco_bairro);
        $("input[name='lenderecoCidade']").val(data.endereco_cidade);
        $("input[name='lenderecoComplemento']").val(data.complemento);
        $("input[name='lvendedor']").val(data.vendedor);
        $("input[name='lpagamentoSituacao']").val(data.situacao_pagamento);
        $("input[name='lnomecartao']").val(data.nome_cartao);
        $("input[name='lnumerocartao']").val(data.numero_cartao);
        $("input[name='lvencimentoCartao']").val(data.vencimento_cartao);
        $("input[name='lvalorPlano']").val(data.valor_plano);
        $("input[name='lvalorMensal']").val(data.valor_mensal);
        $("textarea[name='taObsVenda']").val(data.observacoes_venda);
        $("textarea[name='taObsCobranca']").val(data.observacoes_cobranca);
        $("textarea[name='taObsAuditoria']").val(data.observacoes_auditoria);

    });
}

function saveClient(acao){

    // $("input[name='lnome-do-cliente']")
    var key = $("span[name='key-cpf']").text();
    // console.log(key);
    var ldatanascimentoParts = $("input[name='ldatanascimento']").val().trim().split('/'); // 13/12/2020
    var ldatanascimento_TS = new Date(ldatanascimentoParts[2], ldatanascimentoParts[1] - 1, ldatanascimentoParts[0]).setHours(3,0,0,0);
    var ldatanascimento = new Date(ldatanascimento_TS);

    var sexo = document.getElementById("lsexo").value;
    var nome = document.getElementById("lnome").value;
    var telefone = document.getElementById("ltelefone").value

    var rua = document.getElementById("lendereco").value
    var numero = document.getElementById("lenderecoNumero").value
    var cep = document.getElementById("lenderecoCEP").value
    var bairro = document.getElementById("lenderecoBairro").value
    var cidade = document.getElementById("lenderecoCidade").value
    var estado = document.getElementById("lenderecoEstado").value
    var complemento = document.getElementById("lenderecoComplemento").value

    var nomeCartao = document.getElementById("lnomecartao").value
    var numeroCartao = document.getElementById("lnumerocartao").value
    var vencimentoCartao = document.getElementById("lvencimentoCartao").value
    var bandeiraCartao = document.getElementById("lbandeiraCartao").value
    var valorPlano = document.getElementById("lvalorPlano").value
    var valorMensal = document.getElementById("lvalorMensal").value
    var observacoes_cobranca = document.getElementById("taObsCobranca").value

    var _data = {};

    try {
        _data = {
            updated_date : new Date(),

            nome : nome.trim(),
            sexo : sexo,
            data_nascimento_segurado : ldatanascimento,
            telefone : telefone.trim(),

            endereco_rua : rua.trim(),
            endereco_numero : numero.trim(),
            endereco_bairro : bairro.trim(),
            endereco_cidade : cidade.trim(),
            endereco_cep : cep.trim(),
            endereco_estado : estado.trim(),
            complemento : complemento.trim(),

            nome_cartao : nomeCartao.trim(),
            numero_cartao : numeroCartao.trim(),
            vencimento_cartao : vencimentoCartao.trim(),
            bandeira_cartao : bandeiraCartao.trim(),
            valor_mensal : valorMensal,
            valor_plano : valorPlano,
            observacoes_cobranca : observacoes_cobranca.trim()

        };
    } catch (error) {
        console.log(error);
        return false;
    }

    if (acao == "aprovado") {
        _data.status = ClientStatus.COBRANCA_SUCESSO.code;
    } else if (acao == "invalidado") {
        if (_data.observacoes_cobranca == "") {
            $("textarea[name='taObsCobranca']").focus();
            return;
        }

        _data.status = ClientStatus.COBRANCA_FALHA.code;
    } else if (acao == "retornar_venda") {
        if (_data.observacoes_cobranca == "") {
            $("textarea[name='taObsCobranca']").focus();
            return;
        }
        _data.status = ClientStatus.COBRANCA_RETORNO.code;
    } else {
        return;
    }
    showLoader();

    firestore.collection("clientes").doc(key)
    .set(_data, {merge: true})
        .then(function() {
            console.log("Sucesso ao enviar: "+_data);
            getAllClients();
            alert("Dados atualizados!!");
            hideLoader();
            $('#staticBackdrop').modal('hide');
        })
        .catch(function(error) {
            console.log(error);
            alert("Falha ao salvar dados do cliente");
            hideLoader();
        })

}

function getAllClients(){

    const getClients = firebase.functions().httpsCallable('getClients');
    const _data = {filtros: filtros};
    showLoader();
    getClients(_data)
        .then(result => {

            if (result.data.error) {
                console.log("FALHA "+result);
                filtros = [{key:"erro"}];

                updateFilterBadges();
                hideLoader();

                return;
            }

            const clientes = result.data.clients;
            var datas = []

            clientes.forEach(cliente => {
                datas.push(cliente);
            });

            updateFilterBadges();
            updateTableWith(datas);

        })
        .catch(e => {
            console.error("Error: ", e);
        });
}

function updateTableWith(datas) {
    var div_table = document.getElementById('tabela_dinamica');
    var rows = '';
    while(datas.length) {
        var data = datas.shift();
        var _date = new Date(data.created_date._seconds * 1000);

        let options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var date = _date.toLocaleString('pt-BR', options);

        //  CPF
        var formattedCPF = data.cpf;
        formattedCPF = formattedCPF.replace(/[^\d]/g, "");
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");

        status = ""
        switch (data.status) {
            case ClientStatus.IMPORTADO_PLANILHA.code: status = ClientStatus.IMPORTADO_PLANILHA.name; break;
            case ClientStatus.RECEM_CADASTRADO.code: status = ClientStatus.RECEM_CADASTRADO.name; break;
            case ClientStatus.AUDITORIA.code: status = ClientStatus.AUDITORIA.name; break;
            case ClientStatus.COBRANCA.code: status = ClientStatus.COBRANCA.name; break;
            case ClientStatus.COBRANCA_RETORNO.code: status = ClientStatus.COBRANCA_RETORNO.name; break;
            case ClientStatus.AUDITORIA_RETORNO.code: status = ClientStatus.AUDITORIA_RETORNO.name; break;
            case ClientStatus.COBRANCA_SUCESSO.code: status = ClientStatus.COBRANCA_SUCESSO.name; break;
            case ClientStatus.COBRANCA_FALHA.code: status = ClientStatus.COBRANCA_FALHA.name; break;
            case ClientStatus.CORRIGIDO_VENDA.code: status = ClientStatus.CORRIGIDO_VENDA.name; break;
            default : status = "STATUS NÃO MAPEADO";
        }
        rows = rows + '<tr><td>'+formattedCPF+'</td><td>'+data.nome+'</td><td>'+data.vendedor+'</td><td>'+date+'</td><td>'+status+'</td><td><button type="button" class="btn btn-primary" name="'+data.id+'" onClick="showModalToCPF(this.name)">Ver detalhes</button></td></tr>';
    }
    var html = '<table id="clientes-data-table" class="table table-striped table-bordered"> <thead>'
    html = html + '<tr> <th data-field="cpf" style="min-width:95px">CPF</th> <th data-field="nome">Nome</th> <th data-field="vendedor">Vendedor(a)</th> <th data-field="data_criacao" style="min-width:70px">Criado em</th> <th data-field="estado">Status</th> <th ></th> </tr>    </thead>'
    html = html + '<tbody id="tbody_index">'+rows+'</tfoot> </table>'
    div_table.innerHTML = html;

    hideLoader();

    $('#clientes-data-table').DataTable({
        pagination: true,
        search: true,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Exibir _MENU_ registros por página",
            "sZeroRecords": "Nenhum resultado encontrado",
            "sEmptyTable": "Nenhum resultado encontrado",
            "sInfo": "Exibindo do _START_ até _END_ de um total de _TOTAL_ registros",
            "sInfoEmpty": "Exibindo do 0 até 0 de um total de 0 registros",
            "sInfoFiltered": "(Filtrado de um total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Próximo",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Ativar para ordenar a columna de maneira ascendente",
                "sSortDescending": ": Ativar para ordenar a columna de maneira descendente"
            }
        }
    });

}

function updateDateOnFirestore() {
    const getClients = firebase.functions().httpsCallable('getClients');
    var allCpfs = [];
    const _data = {filtros: [{key:"byDate", value:3}]};
    getClients(_data)
        .then(result => {

            if (result.data.error) {
                console.log("FALHA");
                console.log(result);
                return undefined;
            }

            const clientes = result.data.clients;
            clientes.forEach(cliente => {
                allCpfs.push(cliente.id);
            });

            updateAll(allCpfs) ;

        })
        .catch(e => {
            console.error("Error: ", e);
            return undefined;
            // alert("Erro");
        });
}

function updateAll(ids){
    ids.forEach(id => {
        var updateData = { reated_date: firebase.firestore.FieldValue.delete(), created_date: new Date(), updated_date : new Date()}
        firestore.collection("clientes").doc(id)
        .set(updateData, {merge: true})
            .then(function() {
                console.log("Sucesso ao enviar: "+id);
            })
            .catch(function(error) {
                console.error("Error adding document: ", error);
            });
    })
}

// ====================================
//              FILTRO
// ====================================

function nameFrom(status) {
    var aux = ""
    switch (status) {
        case ClientStatus.IMPORTADO_PLANILHA.code:
            aux = ClientStatus.IMPORTADO_PLANILHA.name;
            break;
        case ClientStatus.RECEM_CADASTRADO.code:
            aux = ClientStatus.RECEM_CADASTRADO.name;
            break;
        case ClientStatus.CORRIGIDO_VENDA.code:
            aux = ClientStatus.CORRIGIDO_VENDA.name;
            break;
        case ClientStatus.AUDITORIA.code:
            aux = ClientStatus.AUDITORIA.name;
            break;
        case ClientStatus.COBRANCA.code:
            aux = ClientStatus.COBRANCA.name;
            break;
        case ClientStatus.COBRANCA_RETORNO.code:
            aux = ClientStatus.COBRANCA_RETORNO.name;
            break;
        case ClientStatus.AUDITORIA_RETORNO.code:
            aux = ClientStatus.AUDITORIA_RETORNO.name;
            break;
        case ClientStatus.COBRANCA_SUCESSO.code:
            aux = ClientStatus.COBRANCA_SUCESSO.name;
            break;
        case ClientStatus.COBRANCA_FALHA.code:
            aux = ClientStatus.COBRANCA_FALHA.name;
            break;
        default:
            break;
    }
    return aux;
}

function updateFilterBadges(){
    var div_filter = document.getElementById('filtros_badges');
    var html = '';

    filtros.forEach(filtro => {
        html += '   ';
        switch (filtro.key) {
            case 'erro':
                html += '<span class="badge badge-pill badge-danger"> Falha na requisição </span>'
                break;
            case 'interval':
                html += '<span class="badge badge-pill badge-info"> Clientes desde '+filtro.start+' até '+filtro.finish+' </span>';
                break;
            case 'today':
                html += '<span class="badge badge-pill badge-info" > Clientes de hoje </span>'
                break;
            case 'all_period':
                html += '<span class="badge badge-pill badge-info"> Todos os clientes </span>'
                break;

            case 'specific_status':
                filtro.status.forEach((status) => {
                    html += '   ';
                    html += '<span class="badge badge-pill badge-info" style="margin-top: 5px;"> '+nameFrom(status)+' </span>'
                });
                break;
            case 'all_status':
                html += '<span class="badge badge-pill badge-info"> Todos os status </span>'
                break;

            case 'specific_sellers':
                filtro.sellers.forEach((seller) => {
                    html += '   ';
                    html += '<span class="badge badge-pill badge-info" style="margin-top: 5px;"> '+seller+' </span>'
                });
                break;

            case 'all_sellers':
                html += '<span class="badge badge-pill badge-info"> Todas as vendedoras </span>'
                break;
        }
    });
    var html_alldiv = '<h4 class="page-title">Clientes cadastrados: ' + html + "</h4>"
    div_filter.innerHTML = html_alldiv;
}

function updateFilter(){
    const filtro_dia = $('#filtro_dia').val();
    const filtro_seller = $('#filtro_seller').val();
    const filtro_status = $('#filtro_status').val();

    filtros = [];

    switch(filtro_dia){
        case 'time_interval':
            const start = $("input[name='datepicker-autoclose-start']").val();
            const finish = $("input[name='datepicker-autoclose-finish']").val();

            if (start == '') {
                $("input[name='datepicker-autoclose-start']").focus();
                return;
            }

            if (finish == '') {
                $("input[name='datepicker-autoclose-finish']").focus();
                return;
            }

            filter = {key:"interval", start:start, finish:finish};
            filtros.push(filter);
            break;

        case 'today':
            filter = {key:"today"};
            filtros.push(filter);
            break;

        case 'all_period':
            filter = {key:"all_period"};
            filtros.push(filter);
            break;
    }

    switch (filtro_seller) {
        case 'all':
            filter = {key:'all_sellers'};
            filtros.push(filter);
            break;

        case 'pinch_seller':
            var sellers = []
            var checkboxes = document.querySelectorAll('input[name=checkbox_sellers]:checked')

            for (var i = 0; i < checkboxes.length; i++) {
                sellers.push(checkboxes[i].value)
            }

            if (sellers.length == 0) {
                $('#filtro_seller').focus();
                return;
            }

            filter = { key:'specific_sellers', sellers : sellers };
            filtros.push(filter);
            break;
    }

    switch (filtro_status) {
        case 'all':
            filtros.push( {key:"all_status"} );
            break;
        default :
            var checkboxes = document.querySelectorAll('input[name=checkbox_status]:checked')
            checkboxes = Array.from(checkboxes);

            var status = checkboxes.map((CheckedStatus) => {
                return CheckedStatus.value
            })

            if (checkboxes.length > 0) {
                filtros.push( {key:"specific_status", status: status} );
            } else {
                filtros.push( {key:"all_status"} );
            }
            break;
    }
    console.log(filtros)
    $('#filter_staticBackdrop').modal('hide');
    getAllClients();
}

// ====================================
//            MODAL DETALHES
// ====================================

function handleStatusBadge(status){
    $("#badge_base__antiga").hide();
    $("#badge_aguardando_auditoria").hide();
    $("#badge_em_auditoria").hide();
    $("#badge_aguardando_cobranca").hide();
    $("#badge_retornado_auditoria").hide();
    $("#badge_retornado_cobranca").hide();
    $("#badge_cliente_sucesso").hide();
    $("#badge_cliente_invalidado").hide();
    $("#badge_status_invalido").hide();
    $("#badge_corrigido_venda").hide();

    switch (status) {
        case ClientStatus.IMPORTADO_PLANILHA.code:
            $("#badge_base__antiga").show();
            break;
        case ClientStatus.RECEM_CADASTRADO.code:
            $("#badge_aguardando_auditoria").show();
            break;
        case ClientStatus.CORRIGIDO_VENDA.code:
            $("#badge_corrigido_venda").show();
            break;
        case ClientStatus.AUDITORIA.code:
            $("#badge_em_auditoria").show();
            break;
        case ClientStatus.COBRANCA.code:
            $("#badge_aguardando_cobranca").show();
            break;
        case ClientStatus.COBRANCA_RETORNO.code:
            $("#badge_retornado_cobranca").show();
            break;
        case ClientStatus.AUDITORIA_RETORNO.code:
            $("#badge_retornado_auditoria").show();
            break;
        case ClientStatus.COBRANCA_SUCESSO.code:
            $("#badge_cliente_sucesso").show();
            break;
        case ClientStatus.COBRANCA_FALHA.code:
            $("#badge_cliente_invalidado").show();
            break;
        default:
            $("#badge_status_invalido").show();
            break;
    }

}

// ====================================
//              LOADER
// ====================================

function showLoader() {
    $(".loader").fadeIn("slow");
}
function hideLoader() {
    $(".loader").fadeOut("slow");
}
