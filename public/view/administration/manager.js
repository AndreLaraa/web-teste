import { ClientStatus, CPFStatus, Momentum } from '../../Model/Model.js';
import * as Monitor from '../../Util/Monitor.js';
import { firestore , secondReference} from '../../Util/Monitor.js';
import { hideLoader, showLoader } from '../../Util/Util.js';

// DEFINE MOMENTUM
Momentum.ADMCONFIG.fun = initializePage;
Monitor.Define_Momentum(Momentum.ADMCONFIG);

window.logout = logout;
window.changeLoginFlag = changeLoginFlag;
window.cadastrarNovoUsuario = cadastrarNovoUsuario;

function logout(){
    Monitor.logout();
}

showLoader();

function initializePage(){
    updateUI();
}

function changeLoginFlag(to){
    showLoader();

    firestore.collection("configLogin").doc("unico")
    .set({'flag':to}, {merge: true})
    .then(function(doc) {
        console.log("Sucesso ao enviar: "+doc);
    })
    .catch(function(error) {
        console.error("Error ", error);
    })
    .finally(function() {
        hideLoader();
        updateUI();
    });
}

function updateUI(){
    showLoader();

    firestore.collection("configLogin").doc("unico")
    .get()
    .then(function(doc) {
        console.log(doc.data());
        var loginHabilitado = doc.data().flag;

        document.getElementById("disableLogin").disabled = !loginHabilitado;
        document.getElementById("enableLogin").disabled = loginHabilitado;
    }).finally(function() {
        hideLoader();
    });
}

// adiciona no employees e quando for criado lá,
//      então cria uma lista de email que quando for fazer login
//      verifica essa lista

function cadastrarNovoUsuario() {
    showLoader();
    var name = document.getElementById("lname").value
    var email = document.getElementById("lemail").value
    var type = document.getElementById("ltype").value

    var data = {
        name : name,
        email: email,
        type : type,
        password : '12345ABC'
    }

    // VARIFICA SE O EMAIL JA ESTÁ CADASTRADO
    firestore.collection("employees").doc(data.email).get().then(function(doc) {
        if (doc.exists) {
            console.log(doc.data().type)
            hideLoader();
            alert("Email "+data.email+" já cadastrado.");
        } else {
            // ADICIONA O EMAIL NA TABELA DE EMPREGADOS
            firestore.collection("employees").doc(data.email).set(data)
                .then(function() {

                    console.log("Sucesso ao enviar: "+data);
                    // CRIA O USUÁRIO
                    secondReference.auth().createUserWithEmailAndPassword(data.email, data.password)
                        .then((userCredential) => {
                            hideLoader();
                            alert("Sucesso ao cadastrar: "+name);
                            console.log(userCredential);
                            document.getElementById('formCadastroUsuario').reset();
                            return secondReference.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
                                .then(() => {
                                    return secondReference.auth().signOut();
                                });
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                })
                .catch(function(error) {
                    console.log(error);
                    alert("Erro desconhecido. Entre em contato com o gerente.");
                    hideLoader();
                    return;
                });
        }
    });

}