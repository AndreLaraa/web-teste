
var firebaseConfig = {
    apiKey: "AIzaSyDhYuI_h3XSVH-653MjH8BSQSWp_bqv59A",
    authDomain: "teste-web21.firebaseapp.com",
    projectId: "teste-web21",
    storageBucket: "teste-web21.appspot.com",
    databaseURL: "https://teste-web21-default-rtdb.firebaseio.com/",
    messagingSenderId: "247710352014",
    appId: "1:247710352014:web:0c41ea571621f54e663fdf"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();

const cadastrarDadosPessoais = document.querySelector('#cadastrarDadosPessoais');
const inputFileXLSX = document.querySelector('#input-planilha');
$("#progress-input-sheet").hide();

firebase.auth().onAuthStateChanged(user => {
    if(user) {
        console.log("");
    } else {
        window.location = '../login/login.html';
    }
});

function logout(){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(() => {
    return firebase.auth().signOut();
  })
}

function writeClients(data, already_in_database) {
    //      WRITING CLIENTS
    const label_upload_progress = document.getElementById('import-progress-text-gravando');

    writing_count = 0;
    writing_failure = 0;
    to_insert_into_database = [];

    for (i in data) {
        if (!already_in_database.includes(data[i].cpf)) {
            to_insert_into_database.push(data[i]);
        }
    }

    total_to_writing = to_insert_into_database.length;

    for (i in to_insert_into_database) {
        // console.log(to_insert_into_database[i]);

        firestore.collection("clientes").doc(to_insert_into_database[i].cpf)
            .set(to_insert_into_database[i], {merge: false})
                .then(function() {
                    writing_count = writing_count + 1;
                    console.log("Sucesso ao enviar: "+to_insert_into_database[i]);
                })
                .catch(function(error) {
                    writing_failure = writing_failure + 1;
                    console.error("Error adding document: ", error);
                })
                .finally(function() {
                    aux_falha = writing_failure > 0 ? '\n' +"\t Há "+String(writing_failure)+ " falha"+(writing_failure > 1 ? "s" : "") : "";
                    checking_text = String(writing_count)+" de "+String(total_to_writing)+" clientes." + aux_falha;

                    if ( total_to_writing == (writing_count + writing_failure) ) {
                        label_upload_progress.innerHTML = "Gravados "+checking_text;
                        alert("Clientes inseridos! Falhas: "+String(writing_failure));
                        $("#progress-input-sheet").hide();
                        $("#input-sheet").show();
                    } else {
                        label_upload_progress.innerHTML = "(Carregando...) Gravando "+checking_text;
                    }
                });
    }

}

function verifyClientsOnBase(data){
    //       VERIFYING REPEATED CLIENTS
    already_in_database = [];
    to_insert_into_database = [];
    const label_repeats_progress = document.getElementById('import-progress-text-repetidos');

    falhas_get = 0;
    checking_text = ""
    verified = 0;
    total_to_verify = data.length;

    for (i in data) {

        firestore.collection("clientes").doc(data[i].cpf).get().then(function(doc) {
            if (doc.exists) {
                already_in_database.push(doc.id);
            }

        }).catch(function(error) {
            console.log("Error getting document:", error);
            falhas_get = falhas_get + 1;

        }).finally(function(){
            verified = verified + 1;

            aux_falha = falhas_get > 0 ? "\t Há "+String(falhas_get)+ " falha"+(falhas_get > 1 ? "s" : "") : "";

            checking_text = String(verified)+"/"+String(total_to_verify)+' - Já contidos na base ' +'('+already_in_database.length+') :'+ already_in_database.join(', ') + '\n' + aux_falha;
            label_repeats_progress.innerHTML = checking_text;

            if (verified == total_to_verify){
                writeClients(data, already_in_database);
            }
        });
    }
}

inputFileXLSX.addEventListener('input',function(evt){
    var selectedFile = evt.target.files[0];
    var reader = new FileReader();
    $("#progress-input-sheet").show();
    $("#input-sheet").hide();


    reader.onload = function(event) {
        var data = event.target.result;
        var workbook = XLSX.read(data, {
            type: 'binary',
            cellDates:true
        });
        workbook.SheetNames.forEach(function(sheetName) {
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            const label_sheet_progress = document.getElementById('import-progress-text-carregando');

            const total_rows = String(XL_row_object.length);
            var progress_counting = 0;
            var datas = [];

            var falhas_message = "";
            var falhas_count = 0;

            label_sheet_progress.innerHTML = 'Carregando '+total_rows+ " clientes.";

            //       PARSING XLSX
            const currentDate = new Date();

            for(var i in XL_row_object){
                try {
                    const val = XL_row_object[i];
                    const _cpf = String(val["CPF"]).trim().split('.').join('').split('-').join('');
                    var observacoes_cobranca = String(val["Status de Cobrança"]) == undefined ? "" : String(val["Status de Cobrança"]).trim();

                    var email = String(val["eMail"]) == undefined ? "" : String(val["eMail"]).trim();
                    var observacoes_venda = String(val["Observações Venda"]) == undefined ? "" : String(val["Observações Venda"]).trim();

                    if (email != "") {
                        email = "| Email : "+email;
                        observacoes_venda = observacoes_venda+email;
                        observacoes_cobranca = "CODIGO714";
                    }


                    const _data = {
                        nome : String(val["Nome Cliente"]).trim(),
                        cpf : _cpf,
                        created_date: currentDate,
                        updated_date : currentDate,
                        numero_cartao : String(val["Número do Cartão de Crédito"]).trim(),
                        nome_cartao : String(val["Nome no Cartão Crédito"]).trim(),
                        vencimento_cartao : String(val["Vencimento Cartão Crédito"].toString()).trim(),
                        bandeira_cartao : String(val["Bandeira Cartão"]).trim(),
                        valor_mensal : String(val["Valor Mensal"]).trim(),
                        valor_plano : String(val["Valor do Plano"]).trim(),
                        sexo : String(val["Sexo"]).trim(),
                        data_nascimento_segurado : String(val["Data de nascimento do segurado"].toString()).trim(),
                        telefone : String(val["Telefone  de Contato"]).trim(),
                        vendedor : String(val["Vendedor"]).trim(),
                        data_venda : String(val["Data da Venda"]).trim(),
                        endereco_rua : String(val["Endereço"]).trim(),
                        endereco_numero : String(val["Número"]) == undefined ? "": String(val["Número"]).trim(),
                        endereco_bairro : String(val["Bairro"]) == undefined ? "" :  String(val["Bairro"]).trim(),
                        endereco_cidade : String(val["Cidade"]).trim(),
                        endereco_cep : String(val["CEP"]).trim(),
                        endereco_estado : String(val["Estado"]).trim(),
                        complemento : String(val["Complemento"]) == undefined ? "" : String(val["Complemento"]).trim(),
                        
                        // observacoes_cobranca : String(val["Status de Cobrança"]) == undefined ? "" : String(val["Status de Cobrança"]).trim(),
                        // nome_conjuge : String(val["Nome do cônjuge"]) == undefined ? "" : String(val["Nome do cônjuge"]).trim(),
                        
                    };

                    datas.push(_data);
                    // console.log(_data);
                } catch (e) {
                    falhas_count = falhas_count + 1;
                    console.log(e.toString());
                } finally {
                    progress_counting = progress_counting + 1;
                    falhas_message = falhas_count > 0 ? "\tOcorreram "+String(falhas_count)+" falhas ao carregar a planilha." : "";
                    label_sheet_progress.innerHTML = '(Carregando...) Carregando '+String(progress_counting)+ " / " + total_rows + falhas_message;
                }
            }

            label_sheet_progress.innerHTML = 'Carregado '+String(progress_counting)+ " / " + total_rows + falhas_message;

            // verifyClientsOnBase(datas);

            // var meuSet = new Set();
            // for(var i in XL_row_object){
            //     var key = i;
            //     var val = XL_row_object[i];
            //     // console.log(val);
            //     for(var j in val){
            //         var sub_key = j;
            //         var sub_val = val[j];

            //         meuSet.add(sub_key);
            //     }
            // }
            // console.log(meuSet)

        })
    };

    reader.onerror = function(event) {
        console.error("File could not be read! Code " + event.target.error.code);
        label_sheet_progress.innerHTML = 'Não foi possível ler o arquivo';
    };

    reader.readAsBinaryString(selectedFile);
})
