import * as firebaseConfig from '../../Util/FirebaseConfig.js';
import { UserException } from '../../Util/Errors.js';
import * as Util from '../../Util/Util.js';

//###########################
// FIREBASE EXCLUSIVO LOGIN
//###########################
firebase.initializeApp(firebaseConfig.firebaseConfig);
firebase.auth().onAuthStateChanged(user => {
    if(user) {
        verifyCurrentUser()
    } else {
        // tira o loading e libera para login
    }
});

function verifyCurrentUser(){
    firebase.auth().currentUser.getIdTokenResult(true).then((idTokenResult) => {
        if (!!idTokenResult.claims.isAdministrator) {
            window.location = '../administration/dashboardAdm.html';
        } else if ((!!idTokenResult.claims.isAuditing) || (!!idTokenResult.claims.isSeller ||
            (!!idTokenResult.claims.isCollector) ) ){
            verifyLoginFlag(idTokenResult.claims)
        }
    })
    .catch((error) => {
        console.log(error);
    });
}

function verifyLoginFlag(toUser) {

    firebase.firestore().collection("configLogin").doc("unico")
    .get()
    .then(function(doc) {
        var loginHabilitado = doc.data().flag;

        if (loginHabilitado) {
            if (!!toUser.isSeller) {
                window.location = '../seller/dashboardSeller.html';
            } else if (!!toUser.isAuditing) {
                window.location = '../auditing/dashboardAuditing.html';
            }
        } else {
            logout();
            alert("Aguarde liberação do gerente");
        }
    })

}

//###########################

Util.hideLoader();
const loginUsuário = document.querySelector('#loginButton');

loginUsuário.addEventListener('click', () => {
    const email = $('#email').val();
    const password = $('#senha').val();

    Util.showLoader();
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(() => {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((user) => {
            Util.hideLoader();
        })
        .catch((error) => {
            console.log(error);
            Util.hideLoader();
            var errorMessage = error.message;
            alert(errorMessage);

            throw new UserException(errorMessage,error.code);
        });
    })
    .catch((error) => {
        Util.hideLoader()
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage);
        console.log(error);
    });

})

function logout(){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(() => {
    return firebase.auth().signOut();
  })
}


$(".preloader").fadeOut();
