import { ClientStatus, CPFStatus, Momentum } from '../../Model/Model.js';
import * as Monitor from '../../Util/Monitor.js';
import { firestore } from '../../Util/Monitor.js';
import { hideLoader, showLoader } from '../../Util/Util.js';

// DEFINE MOMENTUM
Momentum.SELLER_CADASTRO.fun = getPerfil;
Monitor.Define_Momentum(Momentum.SELLER_CADASTRO);

window.logout = logout;
window.cadastrarNovoCliente = cadastrarNovoCliente;

function logout(){
    Monitor.logout();
}

const cadastrarDadosPessoais = document.querySelector('#cadastrarDadosPessoais');
const inputFileXLSX = document.querySelector('#input-planilha');

// CPF DELEGATE //
ValidacaoCPF.delegate.handleCPFStatus = handleCPFStatus;
ValidacaoCPF.delegate.verifyCPFonBase = verifyCPFonBase;
//////////////////



$("#progress-input-sheet").hide();
$('#lVencimentoCartao').mask("00/00", {placeholder: "MM/YY"});

function getPerfil(){
    firestore.collection("employees").doc(firebase.auth().currentUser.email).get().then(function(doc) {
        firebase.auth().currentUser.nome = doc.data().name;
        firebase.auth().currentUser.type = doc.data().type;
        $('#sellerName').html(doc.data().name);
        hideLoader();
    });
}

//  ==============================
//          VALIDAÇÃO DE CPF
//  ==============================
handleCPFStatus();

function handleCPFStatus(){
    $("#validating_CPFValidation").hide();
    $("#invalidated_CPFValidation").hide();
    $("#validated_CPFValidation").hide();
    $("#invalidatedCPF_CPFValidation").hide();

    switch(ValidacaoCPF.delegate.CPFStatusCode){
        case CPFStatus.VALIDATING.code :
            $("#validating_CPFValidation").show();
            break;
        case CPFStatus.VALIDATED.code :
            $("#validated_CPFValidation").show();
            break;
        case CPFStatus.INVALIDATED.code :
            $("#invalidated_CPFValidation").show();
            break;
        case CPFStatus.WRONG.code :
            $("#invalidatedCPF_CPFValidation").show();
        default: break;
    }
}



//  ==============================
//             CADASTRO
//  ==============================
function cadastrarNovoCliente(){
    if (ValidacaoCPF.delegate.CPFStatusCode == CPFStatus.INVALIDATED.code) {
        document.getElementById("lcpf").focus();
        return;
    }

    showLoader();
    var nome = document.getElementById("lnome").value
    var cpf = document.getElementById("lcpf").value
    var sexo = document.getElementById("lsexo").value
    var nascimento = document.getElementById("dataNascimento").value.split('/');
    var telefone = document.getElementById("ltelefone").value

    var rua = document.getElementById("lrua").value
    var numero = document.getElementById("lnumero").value
    var cep = document.getElementById("lcep").value
    var bairro = document.getElementById("lbairro").value
    var cidade = document.getElementById("lcidade").value
    var estado = document.getElementById("luf").value
    var complemento = document.getElementById("lcomplemento").value

    var nomeCartao = document.getElementById("lnomeCartao").value
    var numeroCartao = document.getElementById("lnumeroCartao").value
    var vencimentoCartao = document.getElementById("lVencimentoCartao").value
    var bandeiraCartao = document.getElementById("lbandeiraCartao").value
    var valorPlano = document.getElementById("lvalorPlano").value
    var valorMensal = document.getElementById("lvalorMensal").value
    var observacoes_venda = document.getElementById("lobservacaoVenda").value

    var _cpf = cpf.trim().split('.').join('').split('-').join('');
    var _data = {};

    // PARA CONSEGUIR FZER BUSCA PELA DATA DE NASCIMENTO: TIMESTAMP
    var _nascimentoTS = new Date(nascimento[2],nascimento[1] - 1, nascimento[0]).setHours(3,0,0,0);
    var _nascimento = new Date(_nascimentoTS);

    try {
        _data = {
            created_date: new Date(),
            updated_date : new Date(),
            status : ClientStatus.RECEM_CADASTRADO.code,

            cpf : _cpf,
            nome : nome.trim(),
            sexo : sexo,
            data_nascimento_segurado : _nascimento,
            telefone : telefone.trim(),

            endereco_rua : rua.trim(),
            endereco_numero : numero.trim(),
            endereco_bairro : bairro.trim(),
            endereco_cidade : cidade.trim(),
            endereco_cep : cep.trim(),
            endereco_estado : estado.trim(),
            complemento : complemento.trim(),

            nome_cartao : nomeCartao.trim(),
            numero_cartao : numeroCartao.trim(),
            vencimento_cartao : vencimentoCartao.trim(),
            bandeira_cartao : bandeiraCartao.trim(),
            valor_mensal : valorMensal,
            valor_plano : valorPlano,
            observacoes_venda : observacoes_venda.trim(),
            vendedor : firebase.auth().currentUser.nome,
            data_venda : new Date()
        };
    } catch (error) {
        console.log(error);
        // return false;
    }


    var id = _cpf;
    console.log(_data);

    firestore.collection("clientes").doc(id)
        .set(_data, {merge: true})
            .then(function() {
                hideLoader();
                alert("Sucesso ao cadastrar: "+nome);
                console.log("Sucesso ao enviar: "+_data);
                document.getElementById('formularioCadastroCliente').reset();
                return true;
            })
            .catch(function(error) {
                console.log(error);
                alert("Erro desconhecido. Entre em contato com o gerente.");
                hideLoader();

                // return false;
            });

}

function verifyCPFonBase(cpf_as_Id){
    document.getElementById("lcpf").readOnly = true;
    firestore.collection("clientes").doc(cpf_as_Id).get().then(function(doc) {
        if (doc.exists) {
            ValidacaoCPF.delegate.CPFStatusCode = CPFStatus.INVALIDATED.code;
        } else {
            ValidacaoCPF.delegate.CPFStatusCode = CPFStatus.VALIDATED.code;
        }
        document.getElementById("lcpf").readOnly = false;
        handleCPFStatus();
    });
}

function writeClients(data, already_in_database) {
    //      WRITING CLIENTS
    const label_upload_progress = document.getElementById('import-progress-text-gravando');

    writing_count = 0;
    writing_failure = 0;
    to_insert_into_database = [];

    for (i in data) {
        if (!already_in_database.includes(data[i].cpf)) {
            to_insert_into_database.push(data[i]);
        }
    }

    total_to_writing = to_insert_into_database.length;

    for (i in to_insert_into_database) {
        // console.log(to_insert_into_database[i]);

        firestore.collection("clientes").doc(to_insert_into_database[i].cpf)
            .set(to_insert_into_database[i], {merge: false})
                .then(function() {
                    writing_count = writing_count + 1;
                    console.log("Sucesso ao enviar: "+to_insert_into_database[i]);
                })
                .catch(function(error) {
                    writing_failure = writing_failure + 1;
                    console.error("Error adding document: ", error);
                })
                .finally(function() {
                    aux_falha = writing_failure > 0 ? '\n' +"\t Há "+String(writing_failure)+ " falha"+(writing_failure > 1 ? "s" : "") : "";
                    checking_text = String(writing_count)+" de "+String(total_to_writing)+" clientes." + aux_falha;

                    if ( total_to_writing == (writing_count + writing_failure) ) {
                        label_upload_progress.innerHTML = "Gravados "+checking_text;
                        alert("Clientes inseridos! Falhas: "+String(writing_failure));
                        $("#progress-input-sheet").hide();
                        $("#input-sheet").show();
                    } else {
                        label_upload_progress.innerHTML = "(Carregando...) Gravando "+checking_text;
                    }
                });
    }

}

function verifyClientsOnBase(data){
    //       VERIFYING REPEATED CLIENTS
    already_in_database = [];
    to_insert_into_database = [];
    const label_repeats_progress = document.getElementById('import-progress-text-repetidos');

    falhas_get = 0;
    checking_text = ""
    verified = 0;
    total_to_verify = data.length;

    for (i in data) {

        firestore.collection("clientes").doc(data[i].cpf).get().then(function(doc) {
            if (doc.exists) {
                already_in_database.push(doc.id);
            }

        }).catch(function(error) {
            console.log("Error getting document:", error);
            falhas_get = falhas_get + 1;

        }).finally(function(){
            verified = verified + 1;

            aux_falha = falhas_get > 0 ? "\t Há "+String(falhas_get)+ " falha"+(falhas_get > 1 ? "s" : "") : "";

            checking_text = String(verified)+"/"+String(total_to_verify)+' - Já contidos na base ' +'('+already_in_database.length+') :'+ already_in_database.join(', ') + '\n' + aux_falha;
            label_repeats_progress.innerHTML = checking_text;

            if (verified == total_to_verify){
                writeClients(data, already_in_database);
            }
        });
    }
}

// inputFileXLSX.addEventListener('input',function(evt){
//     var selectedFile = evt.target.files[0];
//     var reader = new FileReader();
//     $("#progress-input-sheet").show();
//     $("#input-sheet").hide();


//     reader.onload = function(event) {
//         var data = event.target.result;
//         var workbook = XLSX.read(data, {
//             type: 'binary',
//             cellDates:true
//         });
//         workbook.SheetNames.forEach(function(sheetName) {
//             var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
//             const label_sheet_progress = document.getElementById('import-progress-text-carregando');

//             const total_rows = String(XL_row_object.length);
//             var progress_counting = 0;
//             var datas = [];

//             var falhas_message = "";
//             var falhas_count = 0;

//             label_sheet_progress.innerHTML = 'Carregando '+total_rows+ " clientes.";

//             //       PARSING XLSX

//             for(var i in XL_row_object){
//                 try {
//                     const val = XL_row_object[i];
//                     const _cpf = String(val["CPF"]).trim().split('.').join('').split('-').join('');

//                     const _data = {
//                         nome : String(val["Nome Cliente"]).trim(),
//                         cpf : _cpf,
//                         created_date: new Date(),
//                         updated_date : new Date(),
//                         status : ClientStatus.RECEMCADASTRADO.code,
//                         numero_cartao : String(val["Número do Cartão de Crédito"]).trim(),
//                         nome_cartao : String(val["Nome no Cartão Crédito"]).trim(),
//                         vencimento_cartao : String(val["Vencimento Cartão Crédito"].toString()).trim(),
//                         bandeira_cartao : String(val["Bandeira Cartão"]).trim(),
//                         valor_mensal : String(val["Valor Mensal"]).trim(),
//                         valor_plano : String(val["Valor do Plano"]).trim(),
//                         sexo : String(val["Sexo"]).trim(),
//                         data_nascimento_segurado : String(val["Data de nascimento do segurado"].toString()).trim(),
//                         telefone : String(val["Telefone  de Contato"]).trim(),
//                         vendedor : String(val["Vendedor"]).trim(),
//                         data_venda : String(val["Data da Venda"]).trim(),
//                         endereco_rua : String(val["Endereço"]).trim(),
//                         endereco_numero : String(val["Número"]) == undefined ? "": String(val["Número"]).trim(),
//                         endereco_bairro : String(val["Bairro"]) == undefined ? "" :  String(val["Bairro"]).trim(),
//                         endereco_cidade : String(val["Cidade"]).trim(),
//                         endereco_cep : String(val["CEP"]).trim(),
//                         endereco_estado : String(val["Estado"]).trim(),
//                         complemento : String(val["Complemento"]) == undefined ? "" : String(val["Complemento"]).trim(),
//                         observacoes_venda : String(val["Observações Venda"]) == undefined ? "" : String(val["Observações Venda"]).trim()
//                     };

//                     datas.push(_data);
//                     // console.log(_data);
//                 } catch (e) {
//                     falhas_count = falhas_count + 1;
//                     console.log(e.toString());
//                 } finally {
//                     progress_counting = progress_counting + 1;
//                     falhas_message = falhas_count > 0 ? "\tOcorreram "+String(falhas_count)+" falhas ao carregar a planilha." : "";
//                     label_sheet_progress.innerHTML = '(Carregando...) Carregando '+String(progress_counting)+ " / " + total_rows + falhas_message;
//                 }
//             }

//             label_sheet_progress.innerHTML = 'Carregado '+String(progress_counting)+ " / " + total_rows + falhas_message;

//             verifyClientsOnBase(datas);

//             var meuSet = new Set();
//             for(var i in XL_row_object){
//                 var key = i;
//                 var val = XL_row_object[i];
//                 // console.log(val);
//                 for(var j in val){
//                     var sub_key = j;
//                     var sub_val = val[j];

//                     meuSet.add(sub_key);
//                 }
//             }
//             console.log(meuSet)

//         })
//     };

//     reader.onerror = function(event) {
//         console.error("File could not be read! Code " + event.target.error.code);
//         label_sheet_progress.innerHTML = 'Não foi possível ler o arquivo';
//     };

//     reader.readAsBinaryString(selectedFile);
// })