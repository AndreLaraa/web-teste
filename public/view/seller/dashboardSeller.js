import { ClientStatus, CPFStatus, Momentum } from '../../Model/Model.js';
import * as Monitor from '../../Util/Monitor.js';
import { firestore } from '../../Util/Monitor.js';
import { hideLoader, showLoader } from '../../Util/Util.js';

// DEFINE MOMENTUM
Momentum.SELLER_INDEX.fun = getPerfil;
Monitor.Define_Momentum(Momentum.SELLER_INDEX);

window.logout = logout;
window.updateFilter = updateFilter;
window.saveClient = saveClient;
window.showModalToCPF = showModalToCPF;

function logout(){
    Monitor.logout();
}

showLoader();
$("#filter_modal_content").load("../../SharedContent/Filter.html");
$("#details_modal_content").load("./ClientDetails.html");

var filtros = [{key:"today"},{key:"all_status", status: undefined}];
var selectedClient;

function getPerfil(){
    showLoader();
    firestore.collection("employees").doc(firebase.auth().currentUser.email).get().then(function(doc) {
        firebase.auth().currentUser.nome = doc.data().name;
        firebase.auth().currentUser.type = doc.data().type;
        $('#sellerName').html(doc.data().name);
        getAllClients();
    });
}

function showModalToCPF(cpf) {

    $('#staticBackdrop').modal('show');

    firestore.collection("clientes").doc(cpf).onSnapshot(function(doc) {
        var data =  doc.data();

        handleStatusBadge(data.status);
        selectedClient = data;
        if (data.status != undefined ){
            var inputs = document.getElementsByClassName("verificado_status");

            switch (data.status) {
                case ClientStatus.RECEM_CADASTRADO.code:
                case ClientStatus.COBRANCA_RETORNO.code:
                case ClientStatus.AUDITORIA_RETORNO.code:
                case ClientStatus.CORRIGIDO_VENDA.code:
                    inputs.forEach(function(input) {
                        input.disabled = false;
                    });
                    document.getElementById("confirmButton").innerHTML = "Corrigir";
                    break;
                default:
                    inputs.forEach(function(input) {
                        input.disabled = true;
                    });

                    break;
            }
        }

        var formattedCPF = data.cpf;
        formattedCPF = formattedCPF.replace(/[^\d]/g, "");
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");

        var _date = new Date(data.data_nascimento_segurado.seconds * 1000);

        let options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var date = _date.toLocaleString('pt-BR', options);

        console.log(data);
        document.getElementById('lsexo').value=data.sexo;
        document.getElementById('lenderecoEstado').value=data.endereco_estado;
        document.getElementById('lbandeiraCartao').value=data.bandeira_cartao;

        $('#modal_client_name').html(data.nome);
        $("#modal_client_cpf").html(formattedCPF);
        $("span[name='key-cpf']").html(doc.id);
        $("input[name='lnome-do-cliente']").val(data.nome);
        $("input[name='lcpf']").val(formattedCPF);

        $("input[name='lnome']").val(data.nome);
        $("input[name='ltelefone']").val(data.telefone);
        $("input[name='ldatanascimento']").val(date);
        $("input[name='lendereco']").val(data.endereco_rua);
        $("input[name='lenderecoNumero']").val(data.endereco_numero);
        $("input[name='lenderecoCEP']").val(data.endereco_cep);
        $("input[name='lenderecoBairro']").val(data.endereco_bairro);
        $("input[name='lenderecoCidade']").val(data.endereco_cidade);
        $("input[name='lenderecoComplemento']").val(data.complemento);
        $("input[name='lvendedor']").val(data.vendedor);
        $("input[name='lpagamentoSituacao']").val(data.situacao_pagamento);
        $("input[name='lnomecartao']").val(data.nome_cartao);
        $("input[name='lnumerocartao']").val(data.numero_cartao);
        $("input[name='lvencimentoCartao']").val(data.vencimento_cartao);
        $("input[name='lvalorPlano']").val(data.valor_plano);
        $("input[name='lvalorMensal']").val(data.valor_mensal);
        $("textarea[name='taObsVenda']").val(data.observacoes_venda);
        $("textarea[name='taObsCobranca']").val(data.observacoes_cobranca);
        $("textarea[name='taObsAuditoria']").val(data.observacoes_auditoria);
    });
}

function saveClient(){
    showLoader();

    // $("input[name='lnome-do-cliente']")
    var key = $("span[name='key-cpf']").text();
    // console.log(key);
    var ldatanascimentoParts = $("input[name='ldatanascimento']").val().trim().split('/'); // 13/12/2020
    var ldatanascimento_TS = new Date(ldatanascimentoParts[2], ldatanascimentoParts[1] - 1, ldatanascimentoParts[0]).setHours(3,0,0,0);
    var ldatanascimento = new Date(ldatanascimento_TS);

    const sexo = document.getElementById("lsexo").value;
    const nome = document.getElementById("lnome").value;
    const telefone = document.getElementById("ltelefone").value

    const rua = document.getElementById("lendereco").value
    const numero = document.getElementById("lenderecoNumero").value
    const cep = document.getElementById("lenderecoCEP").value
    const bairro = document.getElementById("lenderecoBairro").value
    const cidade = document.getElementById("lenderecoCidade").value
    const estado = document.getElementById("lenderecoEstado").value
    const complemento = document.getElementById("lenderecoComplemento").value

    const nomeCartao = document.getElementById("lnomecartao").value
    const numeroCartao = document.getElementById("lnumerocartao").value
    const vencimentoCartao = document.getElementById("lvencimentoCartao").value
    const bandeiraCartao = document.getElementById("lbandeiraCartao").value
    const valorPlano = document.getElementById("lvalorPlano").value
    const valorMensal = document.getElementById("lvalorMensal").value
    const observacoes_venda = document.getElementById("taObsVenda").value

    var _data = {};

    try {
        _data = {
            updated_date : new Date(),

            nome : nome.trim(),
            sexo : sexo,
            data_nascimento_segurado : ldatanascimento,
            telefone : telefone.trim(),

            endereco_rua : rua.trim(),
            endereco_numero : numero.trim(),
            endereco_bairro : bairro.trim(),
            endereco_cidade : cidade.trim(),
            endereco_cep : cep.trim(),
            endereco_estado : estado.trim(),
            complemento : complemento.trim(),

            nome_cartao : nomeCartao.trim(),
            numero_cartao : numeroCartao.trim(),
            vencimento_cartao : vencimentoCartao.trim(),
            bandeira_cartao : bandeiraCartao.trim(),
            valor_mensal : valorMensal,
            valor_plano : valorPlano,
            observacoes_venda : observacoes_venda.trim()

        };
    } catch (error) {
        console.log(error);
        hideLoader();
        return false;
    }

    switch (selectedClient.status) {
        case ClientStatus.COBRANCA_RETORNO.code:
        case ClientStatus.AUDITORIA_RETORNO.code:
        case ClientStatus.CORRIGIDO_VENDA.code:
            _data.status = ClientStatus.CORRIGIDO_VENDA.code;
            break;
        default:
            _data.status = ClientStatus.RECEM_CADASTRADO.code;
            break;
    }

    //RFXX. N-ésima venda
    switch (selectedClient.status) {
        case ClientStatus.COBRANCA_RETORNO.code:
            _data.flowCount = selectedClient.flowCount + 1
            break
        default:
            _data.flowCount = 1

    console.log(_data);

    firestore.collection("clientes").doc(key)
    .set(_data, {merge: true})
        .then(function() {
            console.log("Sucesso ao enviar: "+_data);
            alert("Dados atualizados!!");
            hideLoader();
            $('#staticBackdrop').modal('hide');
        })
        .catch(function(error) {
            console.log(error);
            alert("Falha ao salvar dados do cliente");
            hideLoader();
        })

}

function getAllClients(){

    const getClients = firebase.functions().httpsCallable('getClients');
    // var filtro = filtros.length == 0 ? undefined : filtros;
    console.log( firebase.auth().currentUser.nome)
    const _data = {name: firebase.auth().currentUser.nome, filtros: filtros};
    console.log(_data);
    showLoader();
    getClients(_data)
        .then(result => {

            if (result.data.error) {
                console.log("FALHA");
                console.log(result);

                filtros = [{key:"erro"}];

                updateFilterBadges();
                hideLoader();
                return;
            }

            const clientes = result.data.clients;
            var datas = []
            // console.log(clientes[0]);
            clientes.forEach(cliente => {
                datas.push(cliente);
            });

            updateFilterBadges();
            updateTableWith(datas);

        })
        .catch(e => {
            console.error("Error: ", e);
            filtros = [{key:"erro"}];

            updateFilterBadges();
            updateTableWith([]);
            hideLoader();
            // alert("Erro");
        });

}

function updateTableWith(datas) {
    var div_table = document.getElementById('tabela_dinamica');
    var rows = '';
    while(datas.length) {
        var data = datas.shift();
        var _date = new Date(data.created_date._seconds * 1000);

        let options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var date = _date.toLocaleString('pt-BR', options);

        //  CPF
        var formattedCPF = data.cpf;
        formattedCPF = formattedCPF.replace(/[^\d]/g, "");
        formattedCPF = formattedCPF.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
        status = ""
        switch (data.status) {
            case ClientStatus.IMPORTADO_PLANILHA.code: status = ClientStatus.IMPORTADO_PLANILHA.name; break;
            case ClientStatus.RECEM_CADASTRADO.code: status = ClientStatus.RECEM_CADASTRADO.name; break;
            case ClientStatus.AUDITORIA.code: status = ClientStatus.AUDITORIA.name; break;
            case ClientStatus.COBRANCA.code: status = ClientStatus.COBRANCA.name; break;
            case ClientStatus.COBRANCA_RETORNO.code: status = ClientStatus.COBRANCA_RETORNO.name; break;
            case ClientStatus.AUDITORIA_RETORNO.code: status = ClientStatus.AUDITORIA_RETORNO.name; break;
            case ClientStatus.COBRANCA_SUCESSO.code: status = ClientStatus.COBRANCA_SUCESSO.name; break;
            case ClientStatus.COBRANCA_FALHA.code: status = ClientStatus.COBRANCA_FALHA.name; break;
            case ClientStatus.CORRIGIDO_VENDA.code: status = ClientStatus.CORRIGIDO_VENDA.name; break;
        }

        rows = rows + '<tr><td>'+formattedCPF+'</td><td>'+data.nome+'</td><td>'+date+'</td><td>'+status+'</td><td><button type="button" class="btn btn-primary" name="'+data.id+'" onClick="showModalToCPF(this.name)">Ver detalhes</button></td></tr>';
    }
    var html = '<table id="clientes-data-table" class="table table-striped table-bordered"> <thead>'
    html = html + '<tr> <th data-field="cpf" style="min-width:95px">CPF</th> <th data-field="nome">Nome</th> <th data-field="data_criacao" style="min-width:70px">Criado em</th> <th data-field="estado">Status</th> <th ></th> </tr>    </thead>'
    html = html + '<tbody id="tbody_index">'+rows+'</tfoot> </table>'
    div_table.innerHTML = html;

    hideLoader();

    $('#clientes-data-table').DataTable({
        pagination: true,
        search: true,
        "columns": [
            null, // cpf
            null, // nome
            { "type": "date-eu" }, // criado em
            null, //
            null
        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Exibir _MENU_ registros por página",
            "sZeroRecords": "Nenhum resultado encontrado",
            "sEmptyTable": "Nenhum resultado encontrado",
            "sInfo": "Exibindo do _START_ até _END_ de um total de _TOTAL_ registros",
            "sInfoEmpty": "Exibindo do 0 até 0 de um total de 0 registros",
            "sInfoFiltered": "(Filtrado de um total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Próximo",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Ativar para ordenar a columna de maneira ascendente",
                "sSortDescending": ": Ativar para ordenar a columna de maneira descendente"
            }
        }
    });

}

// ====================================
//            MODAL DETALHES
// ====================================

function handleStatusBadge(status){
    $("#badge_base__antiga").hide();
    $("#badge_aguardando_auditoria").hide();
    $("#badge_em_auditoria").hide();
    $("#badge_aguardando_cobranca").hide();
    $("#badge_retornado_auditoria").hide();
    $("#badge_retornado_cobranca").hide();
    $("#badge_cliente_sucesso").hide();
    $("#badge_cliente_invalidado").hide();
    $("#badge_status_invalido").hide();
    $("#badge_corrigido_venda").hide();

    switch (status) {
        case ClientStatus.IMPORTADO_PLANILHA.code:
            $("#badge_base__antiga").show();
            break;
        case ClientStatus.RECEM_CADASTRADO.code:
            $("#badge_aguardando_auditoria").show();
            break;
        case ClientStatus.CORRIGIDO_VENDA.code:
            $("#badge_corrigido_venda").show();
            break;
        case ClientStatus.AUDITORIA.code:
            $("#badge_em_auditoria").show();
            break;
        case ClientStatus.COBRANCA.code:
            $("#badge_aguardando_cobranca").show();
            break;
        case ClientStatus.COBRANCA_RETORNO.code:
            $("#badge_retornado_cobranca").show();
            break;
        case ClientStatus.AUDITORIA_RETORNO.code:
            $("#badge_retornado_auditoria").show();
            break;
        case ClientStatus.COBRANCA_SUCESSO.code:
            $("#badge_cliente_sucesso").show();
            break;
        case ClientStatus.COBRANCA_FALHA.code:
            $("#badge_cliente_invalidado").show();
            break;
        default:
            $("#badge_status_invalido").show();
            break;
    }

}

// ====================================
//              FILTRO
// ====================================

function updateFilter(){
    const filtro_dia = $('#filtro_dia').val();
    const filtro_status = $('#filtro_status').val();

    filtros = [];

    switch(filtro_dia){
        case 'time_interval':
            const start = $("input[name='datepicker-autoclose-start']").val();
            const finish = $("input[name='datepicker-autoclose-finish']").val();

            if (start == '') {
                $("input[name='datepicker-autoclose-start']").focus();
                return;
            }

            if (finish == '') {
                $("input[name='datepicker-autoclose-finish']").focus();
                return;
            }

            filtros.push( {key:"interval", start:start, finish:finish} );
            break;
        case 'today':
            filtros.push( {key:"today"} );
            break
        case 'all_period':
            filtros.push( {key:"all_period"} );
            break
    }

    switch (filtro_status) {
        case 'all':
            filtros.push( {key:"all_status"} );
            break;
        default :
            var checkboxes = document.querySelectorAll('input[name=checkbox_status]:checked')
            checkboxes = Array.from(checkboxes);

            var status = checkboxes.map((CheckedStatus) => {
                return CheckedStatus.value
            })

            if (checkboxes.length > 0) {
                filtros.push( {key:"specific_status", status: status} );
            } else {
                filtros.push( {key:"all_status"} );
            }
            break;
    }

    $('#filter_staticBackdrop').modal('hide');
    getAllClients();
}

function nameFrom(status) {
    var aux = ""
    switch (status) {
        case ClientStatus.IMPORTADO_PLANILHA.code:
            aux = ClientStatus.IMPORTADO_PLANILHA.name;
            break;
        case ClientStatus.RECEM_CADASTRADO.code:
            aux = ClientStatus.RECEM_CADASTRADO.name;
            break;
        case ClientStatus.CORRIGIDO_VENDA.code:
            aux = ClientStatus.CORRIGIDO_VENDA.name;
            break;
        case ClientStatus.AUDITORIA.code:
            aux = ClientStatus.AUDITORIA.name;
            break;
        case ClientStatus.COBRANCA.code:
            aux = ClientStatus.COBRANCA.name;
            break;
        case ClientStatus.COBRANCA_RETORNO.code:
            aux = ClientStatus.COBRANCA_RETORNO.name;
            break;
        case ClientStatus.AUDITORIA_RETORNO.code:
            aux = ClientStatus.AUDITORIA_RETORNO.name;
            break;
        case ClientStatus.COBRANCA_SUCESSO.code:
            aux = ClientStatus.COBRANCA_SUCESSO.name;
            break;
        case ClientStatus.COBRANCA_FALHA.code:
            aux = ClientStatus.COBRANCA_FALHA.name;
            break;
        default:
            break;
    }
    return aux;
}

function updateFilterBadges(){
    var div_filter = document.getElementById('filtros_badges');
    var html = '';

    filtros.forEach(filtro => {
        html += '   ';
        switch (filtro.key) {
            case 'erro':
                html += '<span class="badge badge-pill badge-danger"> Falha na requisição </span>'
                break;
            case 'interval':
                html += '<span class="badge badge-pill badge-info"> Clientes desde '+filtro.start+' até '+filtro.finish+' </span>';
                break;
            case 'today':
                html += '<span class="badge badge-pill badge-info" > Clientes de hoje </span>'
                break;
            case 'all_period':
                html += '<span class="badge badge-pill badge-info"> Todos os clientes </span>'
                break;
            case 'specific_status':
                filtro.status.forEach((status) => {
                    html += '   ';
                    html += '<span class="badge badge-pill badge-info" style="margin-top: 5px;"> '+nameFrom(status)+' </span>'
                });
                break;

            case 'all_status':
                html += '<span class="badge badge-pill badge-info"> Todos os status </span>'
                break;
        }
    });
    var html_alldiv = '<h4 class="page-title">Clientes cadastrados: ' + html + "</h4>"
    div_filter.innerHTML = html_alldiv;
}
