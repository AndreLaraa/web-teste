export var ClientStatus = {
    IMPORTADO_PLANILHA : {name: "IMPORTADO DA PLANILHA", code: "IP"},
    RECEM_CADASTRADO : {name: "RECÉM CADASTRADO", code: "RC"},
    AUDITORIA       : {name: "EM AUDITORIA", code: "EA"},
    COBRANCA       : {name: "EM COBRANCA", code: "EC"},
    COBRANCA_RETORNO       : {name: "RETORNADO PELA COBRANÇA", code: "RPC"},
    AUDITORIA_RETORNO       : {name: "RETORNADO PELA AUDITORIA", code: "RPA"},
    COBRANCA_SUCESSO       : {name: "COBRADO COM SUCESSO", code: "CS"},
    COBRANCA_FALHA       : {name: "FALHA NA COBRANÇA", code: "CF"},
    CORRIGIDO_VENDA     : {name: "CORREÇÃO EFETUADA", code: "CE"}

};

export var CPFStatus = {
    NILL:{code: 0}, VALIDATING:{code:1}, VALIDATED:{code:2}, INVALIDATED:{code:3}, WRONG:{code:4}
};

export var Momentum = {
    UNDEFINED : undefined,
    SELLER_CADASTRO : {description: "Tela de cadastro", fun: undefined},
    SELLER_INDEX : {description: "Tela de lista de clientes", fun: undefined},
    ADMCONFIG : {description: "Tela de configuração do Adm", fun: undefined}
}