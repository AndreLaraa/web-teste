 /* eslint-disable */

const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp(functions.config().firebase);

// PAGINAÇÃO: https://firebase.google.com/docs/firestore/query-data/query-cursors
exports.getClients = functions.https.onCall((data, context) => {
    if (context.auth == undefined) {
        console.log("ERROR 50478");
        return {error: true, message: "ERROR 50478"};
    }

    var ref = admin.firestore().collection('clientes');
    var start = new Date();
    start.setHours(3,0,0,0);


    data.filtros.forEach(filtro => {
    // Filtro por status
        if (filtro.key == "specific_status") {
            ref = ref.where('status', 'in', filtro.status);
        }

        if (filtro.key == "all_status") {
            console.log("filtro - todos os status");
            // Sem ajuste de status
        }

    // Filtro de data
        if (filtro.key == "interval") {
            console.log("filtro - intervalo "+filtro);

            var startParts = filtro.start.split('/'); // 13/12/2020
            var finishParts = filtro.finish.split('/');

            var startDate_TS = new Date(startParts[2], startParts[1] - 1, startParts[0]).setHours(3,0,0,0);
            var finishDate_TS = new Date(finishParts[2], finishParts[1] - 1, finishParts[0]).setHours(24,0,0,0);

            var startDate = new Date(startDate_TS);
            var finishDate = new Date(finishDate_TS);

            console.log(startDate);
            console.log(finishDate);

            ref = ref.where('created_date', '>=', startDate).where('created_date', '<=', finishDate);

        }

        if (filtro.key == "today") {
            console.log("filtro - hoje");
            ref = ref.where('created_date', '>=', start);

        }

        if (filtro.key == "all_period") {
            console.log("filtro - todo o período");
            // Sem ajuste no horário
        }

    });

    if (context.auth.token.isAdministrator) {
        console.log("PEGANDO CLIENTES PARA ADMINISTRADOR");

        // Filtro de vendedora
        if (data.filtros != undefined) {
            data.filtros.forEach(filtro => {
                if (filtro.key == "specific_sellers") {
                    console.log("filtro - specific_sellers "+filtro.sellers);
                    ref = ref.where('vendedor', 'in', filtro.sellers);
                }
            });
        }

        return ref.orderBy("created_date","desc").get().then(snapshot => {
            var _clients = [];
            snapshot.forEach(doc => {

                var _client = doc.data();
                _client.id = doc.id;
                _client.vendedor = _client.vendedor == undefined ? '' : _client.vendedor;
                _client.uf = _client.endereco_estado == undefined ? '' : _client.endereco_estado;
                _clients.push(_client);
            });
            return {error: false, clients: _clients};
        })
        .catch(err => {
            console.log('Error getting documents', err);
            return {error: true, e: err};
        });

    } else if (context.auth.token.isSeller) {
        console.log("PEGANDO CLIENTES PARA VENDEDORA "+data.name);

        return ref.where("vendedor", "==", data.name).orderBy("created_date", "desc").get().then(snapshot => {
            var _clients = [];
            snapshot.forEach(doc => {
                var _client = doc.data();
                _client.id = doc.id;
                _client.vendedor = _client.vendedor == undefined ? '' : _client.vendedor;
                _client.uf = _client.endereco_estado == undefined ? '' : _client.endereco_estado;
                _clients.push(_client);
            });
            return {error: false, clients: _clients};
        })
        .catch(err => {
            console.log('Error getting documents', err);
            return {error: true, e: err};
        });
    } else if (context.auth.token.isAuditing) {
        console.log("PEGANDO CLIENTES PARA AUDITORIA ");

        // Filtro de vendedora
        if (data.filtros != undefined) {
            data.filtros.forEach(filtro => {
                if (filtro.key == "specific_sellers") {
                    console.log("filtro - specific_sellers "+filtro.sellers);
                    ref = ref.where('vendedor', 'in', filtro.sellers);
                }
            });
        }
        return ref.orderBy("created_date", "desc").get().then(snapshot => {
            var _clients = [];
            snapshot.forEach(doc => {
                var _client = doc.data();
                _client.id = doc.id;
                _client.vendedor = _client.vendedor == undefined ? '' : _client.vendedor;
                _client.uf = _client.endereco_estado == undefined ? '' : _client.endereco_estado;
                _clients.push(_client);
            });
            return {error: false, clients: _clients};
        })
        .catch(err => {
            console.log('Error getting documents', err);
            return {error: true, e: err};
        });
    }
});

exports.addDefaultUserRole = functions.auth.user().onCreate((user) => {

    let uid = user.uid;
    return admin.firestore().collection("employees").doc(user.email)
                .get().then(function(doc) {
                    switch(doc.data().type){
                        case "venda":
                            return admin.auth().setCustomUserClaims(uid,{
                                isSeller: true
                            });
                        case "auditoria":
                            return admin.auth().setCustomUserClaims(uid,{
                                isAuditing: true
                            });
                        case "cobranca":
                            return admin.auth().setCustomUserClaims(uid,{
                                isCollector: true
                            });
                        case "administracao":
                            return admin.auth().setCustomUserClaims(uid,{
                                isAdministrator: true
                            });
                        default:
                            return null
                    }
                });
});